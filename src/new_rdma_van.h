/**
*  Implements van using RDMA
*  Author: Liu Chang
*  Email: liuchang1437@outlook.com
*  Date: Dec. 2017
*/
#ifndef PS_NEW_RDMA_VAN_H_
#define PS_NEW_RDMA_VAN_H_
#include <rdma/rdma_cma.h> // rdma channel management
#include <rdma/rdma_verbs.h>
#include <netdb.h> // getaddrinfo
#include <cstring> // memset
#include <cstdlib> // posix_memalign
#include <errno.h>

#include <string>
#include <vector>
#include <map>
#include <list>
#include <queue>
#include <utility> // pair
#include <thread>
#include <mutex>
#include <condition_variable>

#include "ps/internal/van.h"
#include "ps/internal/postoffice.h"
//#include <fcntl.h>
//#include <sys/stat.h>


namespace ps {
/**
* \brief RDMA based implementation
*/
  const unsigned long long kBufferSize = 1ll * (1024 * 1024 * 1024); // 1 GB
  const int kSendDepth = 500;
  const int kRecvDepth = 500;
  const int kSgeEntry = 5;
  const int kTimeOut = 1000; // ms
  const int kCopyBound = 110592;// if data size is larger than kCopyBound, then we just register the memory ( not copy ).
  const int kMetaBufferSize = 50 * 1024 * 1024; // 50MB
  const int kMinrdma_allocateSize = 256; // 256 Bytes
  const int kLogIteration = (1<<20) - 1; // record the largest send bytes
  using chunck = std::pair<char *, struct ibv_mr *>;

  class NewRdmaVan : public Van {
  public:
    NewRdmaVan() { }
    virtual ~NewRdmaVan() { }

  protected:
  enum rdma_message_type {
    kMsgReq,
    kMsgRes,
    kMsgDone
  };  
  struct rdma_message {
    rdma_message_type type;
    struct {
      int length[5]; // kMsgReq, meta, data[0], data[1], data[2], -1, there are at most three data
      // kMsgRes
      struct {
        void *addr[5];
        uint32_t rkey[5];
      } mr;
      // kMsgDone
      int sender;
    } data;
  };
  struct connection {
    struct rdma_cm_id *id;
    struct ibv_qp *qp;
    struct ibv_cq *cq;

    int send_slots, recv_slots;

    struct rdma_message *send_msg;
    struct rdma_message *recv_msg;
    struct ibv_mr *send_msg_mr;
    struct ibv_mr *recv_msg_mr;
        
    std::mutex conn_mutex;
    std::condition_variable conn_cv;
    bool connected;
    bool active_side;
  };
  struct context {
    struct ibv_context *ctx;
    struct ibv_pd *pd;
    struct ibv_cq *cq;
    struct ibv_comp_channel *comp_channel;
    int conn_cnt;

    char *send_buffer;
    struct ibv_mr *send_buffer_mr;
    char *meta_buffer;
    struct ibv_mr *meta_buffer_mr;
  };
  void Start() override {
    if (event_channel_ == nullptr) {
      event_channel_ = rdma_create_event_channel();
      CHECK_NOTNULL(event_channel_);
      // << " failed when creating rdma event channel";
      event_poller_stop_ = false;
      event_poller_thread_ = std::unique_ptr<std::thread>
        (new std::thread(&NewRdmaVan::poll_event, this));
    }
    Van::Start();
  }

  void Stop() override {
    Van::Stop();
    PS_VLOG(1) << my_node_.ShortDebugString() << " is stopping";
    rdma_destroy_id(listener_);
    cq_poller_stop_ = true;
    cq_poller_thread_->join();
    for (auto &i : connections_) {
      for (auto &j : i.second){ 
        rdma_disconnect(j);
      }
    }
    while (num_connections_ > 0) {
    }
    // CHECK_EQ(event_poller_stop_, true);
    event_poller_thread_->join();
    rdma_destroy_event_channel(event_channel_);
    ibv_destroy_cq(context_->cq);
    ibv_dereg_mr(context_->send_buffer_mr);
    deregister_mr();
    free(context_);
  }
  int Bind(const Node &node, int max_retry) override {
    CHECK_EQ(rdma_create_id(event_channel_, &listener_, nullptr, RDMA_PS_TCP), 0)
      << " failed when creating listener.";

    struct sockaddr_in addr;  
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    int port = node.port;
    for (int i = 0; i < max_retry + 1; ++i) {
      addr.sin_port = htons(port);
      if (rdma_bind_addr(listener_, (struct sockaddr *)&addr) == 0) break;
      if (i == max_retry)
        port = -1;
      else
        port += 1;
    }
    int num_nodes = 2 * (Postoffice::Get()->num_servers() +
      Postoffice::Get()->num_workers());
    CHECK_EQ(rdma_listen(listener_, num_nodes), 0)
    // rdma_listen(rdma_cm_id *id, int backlog). 
      << " failed when rdma_listen.";
    return port;
  }
  void Connect(const Node& node) override {
    CHECK_NE(node.id, node.kEmpty);
    CHECK_NE(node.port, node.kEmpty);
    CHECK(node.hostname.size());
    int node_id = node.id;
    // worker doesn't need to connect to the other workers. same for server
    if ((node.role == my_node_.role) &&
      (node.id != my_node_.id)) {
      return;
    }

    struct rdma_cm_id *id;
    CHECK_EQ(rdma_create_id(event_channel_, &id, nullptr, RDMA_PS_TCP), 0)
      << " failed when creating connect id.";
    init_connection(id, true);

    struct addrinfo *addr;  // which header?
    CHECK_EQ(getaddrinfo(node.hostname.c_str(), 
                          std::to_string(node.port).c_str(), 
                          nullptr, &addr), 0)
      << my_node_.ShortDebugString() << " failed when getaddrinfo";
    CHECK_EQ(rdma_resolve_addr(id, nullptr, addr->ai_addr, kTimeOut), 0)
      << my_node_.ShortDebugString() << " failed when resolve addr";
    freeaddrinfo(addr);

    struct connection *conn = (struct connection *)id->context;
    wait_for_connect(conn);
    connections_[node_id].push_back(id);
    PS_VLOG(3) << my_node_.ShortDebugString() << " connected to node: " << node_id;
  }
  void init_connection(struct rdma_cm_id *id, bool active_side) {
    struct connection *conn = new connection;
    id->context = conn;
    conn->id = id;
    conn->connected = false;
    conn->active_side = active_side;
  }
  void wait_for_connect(struct connection *conn) {
    std::unique_lock<std::mutex> lk(conn->conn_mutex);
    conn->conn_cv.wait(lk, [conn] {
      return conn->connected;
    });
  }
  int SendMsg(const Message& msg) override {
    std::lock_guard<std::mutex> send_lk(send_mutex_);
    int recv_id = msg.meta.recver;
    CHECK_NE(recv_id, Meta::kEmpty);
    auto it = connections_.find(recv_id);
    if (it == connections_.end()) {
      LOG(WARNING) << my_node_.ShortDebugString() << " there is no socket to node: " << recv_id;
      return -1;
    }
    struct rdma_cm_id *rdma_id = *it->second.rbegin();
    struct connection *conn = (struct connection *)rdma_id->context;
    // pack meta
    int meta_size;
    char* meta_buf = context_->meta_buffer;
    PackMeta(msg.meta, &meta_buf, &meta_size);
    // send kMsgReq first
    conn->send_msg->type = kMsgReq;
    conn->send_msg->data.length[0] = meta_size;
    for (size_t i = 0; i < msg.data.size(); ++i) {
      conn->send_msg->data.length[i + 1] = msg.data[i].size();
    }
    conn->send_msg->data.length[msg.data.size() + 1] = -1;
    post_send_rdma_msg(conn);
    // poll kMsgRes
    struct ibv_wc wc;
    poll_rdma_msg(conn->cq, &wc);
    CHECK_EQ(conn->recv_msg->type, kMsgRes)
      << my_node_.ShortDebugString() << " receive message type is not kMsgRes";
    conn->send_slots--; // TODO(liuchang): really need this?
    
    // write meta and data
    int send_bytes = meta_size;
    post_rdma_write(conn, meta_buf, send_bytes, conn->recv_msg->data.mr.addr[0],
      conn->recv_msg->data.mr.rkey[0], context_->meta_buffer_mr->lkey);
    char *p = context_->send_buffer;
    int n = msg.data.size();
    if (n) {
      CHECK_GE(n, 2);
      CHECK_LE(n, 3);
      for (int i = 0; i < n; ++i) {
        SArray<char>* data = new SArray<char>(msg.data[i]);
        //PS_VLOG(1) << " data size: " << data->size();
        if (data->size() > kCopyBound) { // just register it
          if (!is_registered(data->data(), data->size())) {
            register_mr(data->data(), data->size());
          }
          post_rdma_write(conn, data->data(), data->size(), conn->recv_msg->data.mr.addr[i+1],
            conn->recv_msg->data.mr.rkey[i+1], addr_mr_[data->data()].first->lkey);
        }
        else {
          memcpy(p, data->data(), data->size());
          post_rdma_write(conn, p, data->size(), conn->recv_msg->data.mr.addr[i+1],
            conn->recv_msg->data.mr.rkey[i+1], context_->send_buffer_mr->lkey);
          p += data->size();
        }       
        send_bytes += data->size();
      }
    }
    CHECK_LE(send_bytes, kBufferSize)
      << my_node_.ShortDebugString() << " send buffer is not enough.";
    // send kMsgDone
    conn->send_msg->type = kMsgDone;
    conn->send_msg->data.mr = conn->recv_msg->data.mr;
    conn->send_msg->data.sender = my_node_.id; 
    post_send_rdma_msg(conn);

    // receive ack
    do {
      int ret;
      while ((ret = ibv_poll_cq(conn->cq, 1, &wc)) == 0) {
      }
      CHECK_GE(ret, 0);
      CHECK_EQ(wc.status, IBV_WC_SUCCESS)
        << my_node_.ShortDebugString() << " wc status is not success";
      if (wc.opcode == IBV_WC_RECV) {
        if (--conn->recv_slots <= 1) {
          post_recv_rdma_msg(conn, kRecvDepth - conn->recv_slots);
          conn->recv_slots = kRecvDepth;
        }
        if (conn->recv_msg->type == kMsgDone) break;
      }
    } while (1);
    CHECK_EQ(conn->recv_msg->type, kMsgDone)
      << my_node_.ShortDebugString() << " should receive kMsgDone";

    conn->send_slots--;
    /* log statics */
    send_cnt_++;
    max_send_bytes_ = send_bytes>max_send_bytes_?send_bytes:max_send_bytes_;
    if((send_cnt_ & kLogIteration) == 0){
        PS_VLOG(1) << my_node_.ShortDebugString() << " max send bytes: " << max_send_bytes_;
    }
    return send_bytes;
  }

  int RecvMsg(Message* msg) override {
    msg->data.clear();
    struct rdma_message recv_msg;
    {
      std::unique_lock<std::mutex> recv_lk(msg_queue_mutex_);
      msg_queue_cv_.wait(recv_lk, [this] {
        return !(msg_queue_.empty());
      });
      recv_msg = std::move(msg_queue_.front());
      msg_queue_.pop();
    }
    auto &header = recv_msg.data;
    size_t recv_bytes = header.length[0];
    char *p = (char *)header.mr.addr[0];
    UnpackMeta(p, recv_bytes, &(msg->meta));
    rdma_deallocate(p);
    msg->meta.sender = header.sender;
    msg->meta.recver = my_node_.id;
    for (int i = 1; header.length[i] != -1; i++) {
      SArray<char> data;
      p = (char *)header.mr.addr[i];
      data.CopyFrom(p, header.length[i]);
      rdma_deallocate(p);
      msg->data.push_back(data);
      recv_bytes += header.length[i];
    }
    //Log("RecvMsg: " + msg->DebugString(), 4);
    return recv_bytes;
  }
  void poll_event() {
    struct rdma_cm_event *event;

    while (!event_poller_stop_ && rdma_get_cm_event(event_channel_, &event) == 0) {
      struct rdma_cm_event event_copy;
      memcpy(&event_copy, event, sizeof(*event));
      rdma_ack_cm_event(event);

      if (event_copy.event == RDMA_CM_EVENT_CONNECT_REQUEST) {
        on_connect_request(event_copy.id);
      }
      else if (event_copy.event == RDMA_CM_EVENT_ADDR_RESOLVED) {
        on_addr_resolved(event_copy.id);
      }
      else if (event_copy.event == RDMA_CM_EVENT_ROUTE_RESOLVED) {
        on_route_resolved(event_copy.id);
      }
      else if (event_copy.event == RDMA_CM_EVENT_ESTABLISHED) {
        on_established(event_copy.id);
      }
      else if (event_copy.event == RDMA_CM_EVENT_DISCONNECTED) {
        on_disconnected(event_copy.id);
      }
      else {
        std::string event_str = rdma_event_str(event_copy.event);
        LOG(ERROR) << my_node_.ShortDebugString() << " unknown event: " << event_str << " with status: " << event_copy.status;
      }
    }
  }
  void on_connect_request(struct rdma_cm_id *id) {
    struct rdma_conn_param cm_params;
    init_connection(id, false);
    build_connection(id, false);
    build_conn_params(&cm_params);
    CHECK_EQ(rdma_accept(id, &cm_params), 0)
      << my_node_.ShortDebugString() << " failed when accept rdma connection";
  }
  void on_addr_resolved(struct rdma_cm_id *id) {
    build_connection(id, true);
    CHECK_EQ(rdma_resolve_route(id, kTimeOut), 0)
      << my_node_.ShortDebugString() << " failed when resolve route";
  }
  void on_route_resolved(struct rdma_cm_id *id) {
    struct rdma_conn_param cm_params;
    build_conn_params(&cm_params);
    CHECK_EQ(rdma_connect(id, &cm_params), 0)
      << my_node_.ShortDebugString() << " failed when rdma connect";
  }
  void on_established(struct rdma_cm_id *id) {
    num_connections_++;
    struct connection *conn = (struct connection *)id->context;
    std::unique_lock<std::mutex> lk(conn->conn_mutex);
    conn->connected = true;
    conn->conn_cv.notify_all();
  }
  void on_disconnected(struct rdma_cm_id *id) {
    struct connection *conn = (struct connection *)id->context;
    rdma_destroy_qp(id);
    ibv_dereg_mr(conn->send_msg_mr);
    ibv_dereg_mr(conn->recv_msg_mr);
    free(conn->send_msg);
    free(conn->recv_msg);
    free(conn);
    rdma_destroy_id(id);
    CHECK_GE(--num_connections_, 0);
    if (num_connections_ == 0) {
      event_poller_stop_ = true;
    }
  }
  void build_conn_params(rdma_conn_param *params) {
    memset(params, 0, sizeof(*params));
    params->retry_count = 7;
    //The maximum number of times that a send operation from the remote peer
    //should be retried on a connection after receiving a receiver not ready 
    //(RNR) error. RNR errors are generated when a send request arrives before
    //a buffer has been posted to receive the incoming data. 
    params->rnr_retry_count = 7; // infinite retry 
  }
  void build_context(struct ibv_context *verbs) {
    if (context_) {
      CHECK_EQ(context_->ctx, verbs)
        << my_node_.ShortDebugString() << " cannot handle events in more than one context.";
      context_->conn_cnt++;
      CHECK_EQ(ibv_resize_cq(context_->cq,
                              context_->conn_cnt * (kSendDepth + kRecvDepth)),
                0)
        << my_node_.ShortDebugString() << " failed when resize cq";
      return;
    }
    context_ = (struct context *)malloc(sizeof(struct context));
    context_->ctx = verbs;
    CHECK_NOTNULL(context_->pd = ibv_alloc_pd(context_->ctx));
    context_->conn_cnt = 1;
    CHECK_NOTNULL(context_->comp_channel = ibv_create_comp_channel(context_->ctx));
    CHECK_NOTNULL(context_->cq = ibv_create_cq(context_->ctx, kSendDepth + kRecvDepth,
                                                NULL, context_->comp_channel, 0));
    posix_memalign((void **)&context_->send_buffer, sysconf(_SC_PAGESIZE), kBufferSize);
    CHECK_NOTNULL(context_->send_buffer_mr = ibv_reg_mr(
      context_->pd,
      context_->send_buffer,
      kBufferSize,
      IBV_ACCESS_LOCAL_WRITE));
    posix_memalign((void **)&context_->meta_buffer, sysconf(_SC_PAGESIZE), kMetaBufferSize);
    CHECK_NOTNULL(context_->meta_buffer_mr = ibv_reg_mr(
      context_->pd,
      context_->meta_buffer,
      kMetaBufferSize,
      IBV_ACCESS_LOCAL_WRITE));
    cq_poller_stop_ = false;
    cq_poller_thread_ = std::unique_ptr<std::thread>(
      new std::thread(&NewRdmaVan::poll_cq, this));
  }
  void build_connection(struct rdma_cm_id *id, bool active_side) {
    build_context(id->verbs);
    struct connection *conn = (struct connection *)id->context;
    if (active_side) {
      CHECK_NOTNULL(conn->cq = ibv_create_cq(context_->ctx, kSendDepth + kRecvDepth,
                                             NULL, context_->comp_channel, 0));
    }
    else {
      conn->cq = context_->cq;
    }
    CHECK_EQ(ibv_req_notify_cq(conn->cq, 0), 0);
    // build queue pair
    struct ibv_qp_init_attr qp_attr;
    memset(&qp_attr, 0, sizeof(qp_attr));
    qp_attr.send_cq = conn->cq;
    qp_attr.recv_cq = conn->cq;
    qp_attr.qp_type = IBV_QPT_RC;
    qp_attr.cap.max_send_wr = kSendDepth;
    qp_attr.cap.max_recv_wr = kRecvDepth;
    qp_attr.cap.max_send_sge = kSgeEntry;
    qp_attr.cap.max_recv_sge = kSgeEntry;
    qp_attr.sq_sig_all = 0;
    CHECK_EQ(rdma_create_qp(id, context_->pd, &qp_attr), 0)
      << my_node_.ShortDebugString() << " failed when create qp";
    conn->qp = id->qp;
    register_memory(conn);
    conn->send_slots = 0;
    conn->recv_slots = kRecvDepth;
    post_recv_rdma_msg(conn, kRecvDepth);
  }
  void register_memory(struct connection *conn) {
    int rdma_message_size = sizeof(struct rdma_message);
    conn->send_msg = (struct rdma_message *)malloc(rdma_message_size);
    CHECK_NOTNULL(conn->send_msg_mr = ibv_reg_mr(
      context_->pd,
      conn->send_msg,
      rdma_message_size,
      IBV_ACCESS_LOCAL_WRITE));
    conn->recv_msg = (struct rdma_message *)malloc(rdma_message_size);
    CHECK_NOTNULL(conn->recv_msg_mr = ibv_reg_mr(
      context_->pd,
      conn->recv_msg,
      rdma_message_size,
      IBV_ACCESS_LOCAL_WRITE));
    //posix_memalign((void **)&conn->recv_buffer, sysconf(_SC_PAGESIZE), kBufferSize);
    //CHECK_NE((conn->recv_buffer_mr = ibv_reg_mr(
    //  context_->pd,
    //  conn->recv_buffer,
    //  kBufferSize,
    //  IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE)), nullptr)
    //  << my_node_.ShortDebugString() << " failed when register memory with errno: " << errno;
  }
  void post_recv_rdma_msg(struct connection *conn, int n) {
    struct ibv_recv_wr wr, *bad_wr = nullptr;
    struct ibv_sge sge;

    memset(&sge, 0, sizeof(sge));
    sge.addr = (uintptr_t)conn->recv_msg;
    sge.lkey = conn->recv_msg_mr->lkey;
    sge.length = sizeof(struct rdma_message);

    memset(&wr, 0, sizeof(struct ibv_recv_wr));
    wr.wr_id = (uintptr_t)conn;
    wr.next = nullptr;
    wr.sg_list = &sge;
    wr.num_sge = 1;

    for (int i = 0; i < n; ++i) {
      CHECK_EQ(ibv_post_recv(conn->qp, &wr, &bad_wr), 0)
        << my_node_.ShortDebugString() << " failed when post recv with errno: " << errno;
    }
  }
  void post_send_rdma_msg(struct connection *conn, int send_flags = 0) {
    struct ibv_send_wr wr, *bad_wr = nullptr;
    struct ibv_sge sge;

    memset(&sge, 0, sizeof(sge));
    sge.addr = (uintptr_t)conn->send_msg;
    sge.lkey = conn->send_msg_mr->lkey;
    sge.length = sizeof(struct rdma_message);

    memset(&wr, 0, sizeof(struct ibv_send_wr));
    wr.wr_id = (uintptr_t)conn;
    wr.opcode = IBV_WR_SEND;
    wr.sg_list = &sge;
    wr.num_sge = 1;
    wr.send_flags = send_flags;
    while (conn->send_slots >= kSendDepth - 1) {
    }
    conn->send_slots++;
    CHECK_EQ(ibv_post_send(conn->qp, &wr, &bad_wr), 0)
      << my_node_.ShortDebugString() << " failed when post send";
  }
  void poll_rdma_msg(struct ibv_cq *cq, struct ibv_wc *wc) {
    while (1) {
      int ret = 0;
      while ((ret = ibv_poll_cq(cq, 1, wc)) == 0) {
      }
      CHECK_GE(ret, 0) << my_node_.ShortDebugString() << " failed when ibv_poll_cq";
      CHECK_EQ(wc->status, IBV_WC_SUCCESS)
        << my_node_.ShortDebugString() << " wc status is not success";
      struct connection *conn = (struct connection *)wc->wr_id;
      if (wc->opcode == IBV_WC_RECV) {
        if (--conn->recv_slots <= 1) {
          post_recv_rdma_msg(conn, kRecvDepth - conn->recv_slots);
          conn->recv_slots = kRecvDepth;
        }
        return;
      }
      if (wc->opcode == IBV_WC_SEND || wc->opcode == IBV_WC_RDMA_WRITE) conn->send_slots--;
    }
  }
  void post_rdma_write(struct connection *conn, char *send_addr, int send_size, void *peer_addr,
    uint32_t rkey, uint32_t lkey) {
    struct ibv_send_wr wr, *bad_wr = nullptr;
    struct ibv_sge sge;

    memset(&sge, 0, sizeof(sge));
    sge.addr = (uintptr_t)send_addr;
    sge.lkey = lkey;
    sge.length = send_size;

    memset(&wr, 0, sizeof(wr));
    wr.wr_id = (uintptr_t)conn;
    wr.opcode = IBV_WR_RDMA_WRITE;
    wr.next = nullptr;
    wr.sg_list = &sge;
    wr.num_sge = 1;
    wr.wr.rdma.remote_addr = (uintptr_t)peer_addr;
    wr.wr.rdma.rkey = rkey;
    wr.send_flags = IBV_SEND_SIGNALED;

    while (conn->send_slots >= kSendDepth - 1) {
    }
    conn->send_slots++;
    CHECK_EQ(ibv_post_send(conn->qp, &wr, &bad_wr), 0)
      << my_node_.ShortDebugString() << " failed when post rdma write request";

    // poll rdma_write completion
    int ret;
    struct ibv_wc wc;
    do {
      while ((ret = ibv_poll_cq(conn->cq, 1, &wc)) == 0) {
      }
      CHECK_GE(ret, 0);
      CHECK_EQ(wc.status, IBV_WC_SUCCESS)
        << my_node_.ShortDebugString() << " wc status is not success";
      if (wc.opcode == IBV_WC_RECV) { // when would this happen?
        if (--conn->recv_slots <= 1) {
          post_recv_rdma_msg(conn, kRecvDepth - conn->recv_slots);
          conn->recv_slots = kRecvDepth;
        }
      }
    } while (wc.opcode != IBV_WC_RDMA_WRITE);
    CHECK(wc.opcode == IBV_WC_RDMA_WRITE) 
      << my_node_.ShortDebugString() << " opcode is not IBV_WC_RDMA_WRITE";
    conn->send_slots--;
  }
  void poll_cq() {
    struct connection *conn;
    struct ibv_wc wc;

    while (!cq_poller_stop_) {
      int ret = ibv_poll_cq(context_->cq, 1, &wc);
      CHECK_GE(ret, 0) << my_node_.ShortDebugString() << " failed when ibv_poll_cq";

      if (ret == 0) continue;
      CHECK_EQ(wc.status, IBV_WC_SUCCESS)
        << my_node_.ShortDebugString() << " wc status is not success";

      conn = (struct connection *)wc.wr_id;
      if (wc.opcode == IBV_WC_RECV) {
        if (--conn->recv_slots <= 1) {
          post_recv_rdma_msg(conn, kRecvDepth - conn->recv_slots);
          conn->recv_slots = kRecvDepth;
        } 
      }
      else if (wc.opcode == IBV_WC_SEND) {
        conn->send_slots--;
        continue;
      }

      if (conn->recv_msg->type == kMsgReq) {
        // send kMsgRes
        conn->send_msg->type = kMsgRes;
        auto &data = conn->recv_msg->data;
        for (int i = 0; data.length[i] != -1; ++i) {
          auto p = rdma_allocate(data.length[i]);
          conn->send_msg->data.mr.addr[i] = p.first;
          conn->send_msg->data.mr.rkey[i] = p.second->rkey;
        }
        post_send_rdma_msg(conn, IBV_SEND_SIGNALED);
      }
      else if (conn->recv_msg->type == kMsgDone) {
        {
          std::lock_guard<std::mutex> lk(msg_queue_mutex_);
          msg_queue_.push(*conn->recv_msg);
          msg_queue_cv_.notify_all();
        }
        conn->send_msg->type = kMsgDone;
        post_send_rdma_msg(conn, IBV_SEND_SIGNALED);
      }
      else {
        LOG(ERROR) << "Unexpected msg: " << conn->recv_msg->type;
        exit(-1);
      }
    }
  }
  void register_mr(char *addr, size_t size) {
    addr_mr_[addr] = std::make_pair(ibv_reg_mr(context_->pd, addr, size, IBV_ACCESS_LOCAL_WRITE), size);
    CHECK_NOTNULL(addr_mr_[addr].first);
  }
  void deregister_mr() {
    for (const auto &it : addr_mr_) {
      ibv_dereg_mr(it.second.first);
    }
    for (const auto &it : free_memory_chunck_) {
      for (auto &p : it.second) {
        ibv_dereg_mr(p.second);
        free(p.first);
      }
    }
  }
  bool is_registered(char *addr, size_t size) {
    auto it = addr_mr_.find(addr);
    return it != addr_mr_.end() && it->second.second >= size;
  }
  chunck rdma_allocate(size_t size) {
    std::lock_guard<std::mutex> lk(memory_mutex_);
    size = round_bytes(size);
    auto it = free_memory_chunck_.lower_bound(size);
    if (it == free_memory_chunck_.end() || it->second.empty()) {
      char *p = (char *)malloc(size);
      struct ibv_mr *mr;
      CHECK_NE((mr = ibv_reg_mr(context_->pd, p, size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE)), nullptr)
        << my_node_.ShortDebugString() << " failed when register memory with errno: " << errno;
      auto ret = std::make_pair(p, mr);
      allocated_memory_chunck_[p] = std::make_pair(ret, size);
      return ret;
    }
    else {
      auto ret = it->second.front(); // a free chunck
      allocated_memory_chunck_[ret.first] = std::make_pair(ret, size);
      it->second.pop_front();
      return ret;
    }
  }
  void rdma_deallocate(char *addr) {
    std::lock_guard<std::mutex> lk(memory_mutex_);
    auto it = allocated_memory_chunck_.find(addr);
    free_memory_chunck_[it->second.second].push_back(it->second.first);
    allocated_memory_chunck_.erase(it);
  }
  size_t round_bytes(size_t size) {
    if (size < kMinrdma_allocateSize)
      return kMinrdma_allocateSize;
    return kMinrdma_allocateSize * ((size + kMinrdma_allocateSize - 1) / kMinrdma_allocateSize);
  }

private:
  // for connection
  struct rdma_event_channel *event_channel_ = nullptr;
  struct rdma_cm_id *listener_ = nullptr;
  volatile bool event_poller_stop_ = false;
  std::unique_ptr<std::thread> event_poller_thread_;
  volatile bool cq_poller_stop_ = false;
  std::unique_ptr<std::thread> cq_poller_thread_;
  std::unordered_map<int, std::vector<rdma_cm_id *>> connections_;
  struct context *context_ = nullptr;
  volatile int num_connections_ = 0;
  
  // for message transmission
  std::mutex send_mutex_;
  std::mutex msg_queue_mutex_;
  std::condition_variable msg_queue_cv_;
  std::queue<struct rdma_message> msg_queue_;

  // for memory management
  std::mutex memory_mutex_;
  std::map<char *, std::pair<struct ibv_mr *, size_t>> addr_mr_;
  std::map<size_t, std::list<chunck>> free_memory_chunck_;
  std::map<char *, std::pair<chunck, size_t>> allocated_memory_chunck_;
  // static
  int max_send_bytes_ = 0;
  int send_cnt_ = 0;
};
}  // namespace ps
#endif
