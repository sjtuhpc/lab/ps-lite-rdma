# RDMA-enabled ps-lite

We provide an `RDMAVan` for ps-lite, which transfers messages by RDMA.

## How to merge it into MXNet?

1. Just replace `MXNet/ps-lite` with our ps-lite.
2. Add `-lrdmacm -libverbs` to the additional link flags.
3. Compile as before.
